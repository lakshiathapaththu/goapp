package main

import (
	"fmt"
)

func main() {

	var service string
	fmt.Println("Enter the service to be tested: \n   " +
		"Flight Service --> 1 \n   " +
		"Inventory Service --> 2\n   " +
		"Roll Forward Service --> 3\n   " +
		"Flight Inventory Service --> 4\n   " +
		"Roll Forward->Block->On Hold->Sell->Cancel full flow --> 5\n   " +
		"Clean up database --> 6\n   "+
		"Transfer Inventory --> 7\n   " +
		"Go Show --> 8")

	fmt.Scanln(&service)

	if service == "1" {
		InvokeFlightService()

	}else if service == "2" {
		InvokeInventorySerivce()

	}else if service == "3" {
		InvokeRollForwardService()

	}else if service== "4" {
		InvokeFlightInventoryService()

	}else if service == "5"{
		InvokeRollFwdToCancelFlow()

	}else if service == "6"{
		InvokeCleanUpDatabase()
	}else if service == "7" {
		InvokeTransferInventory()
	}else if service == "8" {
		InvokeGoShow()
	}

}