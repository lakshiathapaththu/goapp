package structs

type ModifyInventoryData struct {
	NumberOfRequestsForRF int `json:"numberOfRequestsForRF"`
	NumberOfConcurrentRequestsForRF uint `json:"numberOfConcurrentRequestsForRF"`
	SourceFlightId int32 `json:"sourceFlightId"`
	DateRangeStartingDateForRF string `json:"dateRangeStartingDateForRF"`
	DateRangeSize int `json:"dateRangeSize"`

	NumberOfRequestsForModify int `json:"numberOfRequestsForModify"`
	NumberOfConcurrentRequestsForModify uint `json:"numberOfConcurrentRequestsForModify"`
	FlightNumbers[] string `json:"flightNumbers"`

	NumberOfRequestsForSellInventory           int       `json:"numberOfRequestsForSellInventory"`
	NumberOfConcurrentRequestsForSellInventory uint      `json:"numberOfConcurrentRequestsForSellInventory"`
	BookingClass                               string    `json:"bookingClass"`
	ChildCapacities                            [] string `json:"childCapacities"`
	InfantCapacities                           [] string `json:"infantCapacities"`
	AdultCapacities                            [] string `json:"adultCapacities"`
	DepartureDateLocalStartsWith               string    `json:"departureDateLocalStartsWith"`
	TimePeriod                                 string    `json:"timePeriod"`
	NumberOfRequestsForTransform int `json:"numberOfRequestsForTransform"`
	NumberOfConcurrentRequestsForTransform uint `json:"numberOfConcurrentRequestsForTransform"`

}