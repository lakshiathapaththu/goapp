package report_generation

import (
	"fmt"
	"github.com/bojand/ghz/runner"
)
func PrintReport(report *runner.Report)  {
	fmt.Print("Count:")
	fmt.Print(report.Count)
	fmt.Print("\n")

	fmt.Print("Total:")
	fmt.Print(report.Total)
	fmt.Print("\n")

	fmt.Print("Average:")
	fmt.Print(report.Average)
	fmt.Print("\n")

	fmt.Print("Fastest:")
	fmt.Print(report.Fastest)
	fmt.Print("\n")

	fmt.Print("Slowest:")
	fmt.Print(report.Slowest)
	fmt.Print("\n")

	fmt.Print("Rps:")
	fmt.Print(report.Rps)
	fmt.Print("\n")

	fmt.Print("\n")
}
