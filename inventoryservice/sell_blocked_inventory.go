package inventoryservice

import (
	"fmt"
	aeroinventory "com/accelaero/aeroinventory/inventory"
	"github.com/golang/protobuf/proto"
	"github.com/jhump/protoreflect/desc"
	"os"
	"github.com/bojand/ghz/runner"
	"../report_generation"
)

var blockedIdsR[] string

func sellBlockedInventoryDataFunc(mtd *desc.MethodDescriptor, cd *runner.CallData) []byte {
	invetBlockTransferReq := &aeroinventory.InventoryBlockTransferRequest{}
	invetBlockTransferReq.BlockIds = blockedIdsR

	binData, err := proto.Marshal(invetBlockTransferReq)

	if(err != nil){
		fmt.Println(err.Error())
		os.Exit(1)
	}
	return binData
}

func SellBlockedInventory(numberOfRequests int, numberOfConcurrentRequests uint, blockedIds[] string)  {

	blockedIdsR = blockedIds

	fmt.Print("Sell Blocked Inventory calls with :")

	report, err := runner.Run(
		"aeroinventory.InventoryService.sellBlockedInventory",
		"localhost:6565",
		runner.WithProtoFile(
			"/home/chirantha/codebases/goProj/goProtos/proto/InventoryService.proto",
			[]string{}),
		runner.WithConcurrency(numberOfConcurrentRequests),
		runner.WithInsecure(true),
		runner.WithTotalRequests(uint(numberOfRequests)),
		runner.WithBinaryDataFunc(sellBlockedInventoryDataFunc),
	)

	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	report_generation.PrintReport(report)
}

