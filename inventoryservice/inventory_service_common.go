package inventoryservice

import (
	aeroinventory "com/accelaero/aeroinventory/inventory"
	"fmt"
	"os"
	"strconv"
	"time"
	"github.com/bojand/ghz/runner"

)

var flightNumbersR[] string
var bookingClassR string
var departureDateLocalR string
var numberOfRequestsR int
var numOfReqPerFlightR int

var childCapacitiesRN [][]int
var adultCapacitiesRN [][]int
var infantCapacitiesRN [][]int

func GetCancelAndSellReqObject(cd *runner.CallData) *aeroinventory.FlightSegmentInventory {


	factor := int(cd.RequestNumber)/numOfReqPerFlightR
	res := int(cd.RequestNumber)%numOfReqPerFlightR

	resAd := factor%len(adultCapacitiesRN)
	resCh := factor%len(adultCapacitiesRN)
	resIn := factor%len(adultCapacitiesRN)
	resFn := factor%len(flightNumbersR)

	flighSegmentInventory := &aeroinventory.FlightSegmentInventory{}

	dateAdditionFraction := int(cd.RequestNumber)/(len(flightNumbersR)*numOfReqPerFlightR)

	flightSeg := &aeroinventory.FlightSeg{}
	flightSeg.SegmentCode = "SHJ/CMB"

	layout := "2006-01-02T15:04:05"
	t, err := time.Parse(layout, departureDateLocalR)

	flightSeg.DepartureDateTimeLocal = t.AddDate(0,0, dateAdditionFraction).Format(layout)
	flightSeg.FlightNumber = flightNumbersR[resFn]

	flighSegmentInventory.Segment = flightSeg

	space := &aeroinventory.SpaceRequest{}
	space.AdultCapacity = int32(adultCapacitiesRN[resAd][res])
	space.ChildCapacity = int32(childCapacitiesRN[resCh][res])
	space.InfantCapacity = int32(infantCapacitiesRN[resIn][res])

	space.BookingClass = bookingClassR

	flighSegmentInventory.Space = space

	if(err != nil){
		fmt.Println(err.Error())
		os.Exit(1)
	}

	fmt.Print( strconv.Itoa(int(cd.RequestNumber))+"  "+flighSegmentInventory.Segment.FlightNumber+"  "+
		flighSegmentInventory.Segment.DepartureDateTimeLocal+"  "+
		strconv.Itoa(int(flighSegmentInventory.Space.AdultCapacity))+"  "+
		strconv.Itoa(int(flighSegmentInventory.Space.ChildCapacity))+"  "+
		strconv.Itoa(int(flighSegmentInventory.Space.InfantCapacity))+"\n")
	return flighSegmentInventory
}

func divideCounts(capacitiesInput[] string, numOfReqPerFlightR int) [][]int {
	var dividedCapacityInputs[] []int
	for i := 0; i < len(capacitiesInput); i++ {
		number, err := strconv.ParseInt(capacitiesInput[i],10,64)

		if(err != nil){
			fmt.Println(err.Error())
			os.Exit(1)
		}

		dividedCapacityInputs = append(dividedCapacityInputs,divideNumberIntoParts(int(number) ,numOfReqPerFlightR))
	}
	return dividedCapacityInputs
}


func divideNumberIntoParts(number int, parts int) []int{
	val := number / parts
	var partList[] int
	sum :=0

	if val>0 {
		for i := 1; i <= parts-1; i++ {
			partList = append(partList, val)
			sum += val

		}
		partList = append(partList, number-sum)

	}else {
		for i := 1; i <= number; i++ {
			partList = append(partList, 1)

		}
		rem := parts-len(partList)
		for j := 1; j<=rem; j++{
			partList = append(partList,0)
		}
	}

	return partList
}



