package modifyinventoryservice

import (
	aeroinventory "com/accelaero/aeroinventory"
	modifyinventory "com/accelaero/aeroinventory/modifyinventory"
	"fmt"
	"github.com/bojand/ghz/runner"
	"github.com/golang/protobuf/proto"
	"github.com/jhump/protoreflect/desc"
	"os"
	"strconv"
	"../report_generation"
	"time"
	"../databasecalls"
)

var numberOfRequestsR int
var timePeriodR string
var flightNumbersR[] string
var bookingClassR string
var existingDepartureDateLocalStartsWithR string
var newDepartureDateLocalStartsWithR string
var childCapacitiesR []int
var adultCapacitiesR []int
var infantCapacitiesR []int
var blockIds[] string
var blockIdsPerReq int

func GetModifyInventoryBlockRequest(mtd *desc.MethodDescriptor, cd *runner.CallData) []byte {

	indexFN := int(cd.RequestNumber) % len(flightNumbersR)
	indexAC := int(cd.RequestNumber) % len(adultCapacitiesR)
	indexCC := int(cd.RequestNumber) % len(childCapacitiesR)
	indexIC := int(cd.RequestNumber) % len(infantCapacitiesR)

	modifyInventoryBlockRQ := &modifyinventory.BlockInventoryRQ{}

	modifyInventoryBlockRQ.TimePeriod = timePeriodR

	existingFlightSegmentModifyInventory := modifyinventory.FlightSegmentModifyInventory{}
	existingFlightSegment := modifyinventory.FlightSegment{}
	existingFlightSegment.FlightNumber = flightNumbersR[indexFN]
	existingFlightSegment.SegmentCode = "SHJ/CMB"
	layout := "2006-01-02T15:04:05"
	t1, err := time.Parse(layout, existingDepartureDateLocalStartsWithR)

	if int(cd.RequestNumber) != 0 && int(cd.RequestNumber) % len(flightNumbersR) == 0 {
		existingFlightDepartureDate := t1.AddDate(0, 0, int(cd.RequestNumber)-1)
		existingDepartureDateLocalStartsWithR = existingFlightDepartureDate.Format(layout)
	}

	commonDateExisting := aeroinventory.CommonDate{}
	commonDateExisting.DateText = existingDepartureDateLocalStartsWithR
	existingFlightSegment.DepartureDateTimeLocal = &commonDateExisting
	existingFlightSegmentModifyInventory.FlightSegment = &existingFlightSegment

	existingSpaceRequirement := modifyinventory.SpaceRequirement{}
	existingSpaceRequirement.BookingClass = bookingClassR
	existingSpaceRequirement.Adults = int32(adultCapacitiesR[indexAC])
	existingSpaceRequirement.Children = int32(childCapacitiesR[indexCC])
	existingSpaceRequirement.Infants = int32(infantCapacitiesR[indexIC])

	existingFlightSegmentModifyInventory.SpaceRequirement = &existingSpaceRequirement

	newFlightSegmentModifyInventory := modifyinventory.FlightSegmentModifyInventory{}
	newFlightSegment := modifyinventory.FlightSegment{}
	newFlightSegment.FlightNumber = flightNumbersR[indexFN]
	newFlightSegment.SegmentCode = "SHJ/CMB"

	t2, err := time.Parse(layout, newDepartureDateLocalStartsWithR)
	if int(cd.RequestNumber) != 0 && int(cd.RequestNumber) % len(flightNumbersR) == 0 {
		newFlightDepartureDate := t2.AddDate(0, 0, int(cd.RequestNumber)-1)
		newDepartureDateLocalStartsWithR = newFlightDepartureDate.Format(layout)
	}

	commonDateNew := aeroinventory.CommonDate{}
	commonDateNew.DateText = newDepartureDateLocalStartsWithR
	newFlightSegment.DepartureDateTimeLocal = &commonDateNew
	newFlightSegmentModifyInventory.FlightSegment = &newFlightSegment

	newSpaceRequirement := modifyinventory.SpaceRequirement{}
	newSpaceRequirement.BookingClass = bookingClassR
	newSpaceRequirement.Adults = int32(adultCapacitiesR[indexAC])
	newSpaceRequirement.Children = int32(childCapacitiesR[indexCC])
	newSpaceRequirement.Infants = int32(infantCapacitiesR[indexIC])

	newFlightSegmentModifyInventory.SpaceRequirement = &newSpaceRequirement

	modifyInventoryBlockRQ.ExistingFlightSegmentModifyInventory = []*modifyinventory.FlightSegmentModifyInventory{&existingFlightSegmentModifyInventory}
	modifyInventoryBlockRQ.NewFlightSegmentModifyInventory = []*modifyinventory.FlightSegmentModifyInventory{&newFlightSegmentModifyInventory}

	modifyInventoryBlockRQ.OrderType = modifyinventory.OrderType_SOLD
	modifyInventoryBlockRQ.ReferenceId =  "b3059978-2a89-4538-ac05-a9a83d52e8eb"

	fmt.Print(cd.RequestNumber)
	fmt.Print("  ")
	fmt.Print(modifyInventoryBlockRQ.ExistingFlightSegmentModifyInventory[0].FlightSegment.FlightNumber)
	fmt.Print("  ")
	fmt.Print(modifyInventoryBlockRQ.ExistingFlightSegmentModifyInventory[0].FlightSegment.DepartureDateTimeLocal)
	fmt.Print("  ")
	fmt.Print(modifyInventoryBlockRQ.NewFlightSegmentModifyInventory[0].FlightSegment.FlightNumber)
	fmt.Print("  ")
	fmt.Print(modifyInventoryBlockRQ.NewFlightSegmentModifyInventory[0].FlightSegment.DepartureDateTimeLocal)
	fmt.Print("\n")

	binData, err := proto.Marshal(modifyInventoryBlockRQ)

	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	return binData
}

func transformBlockedInventoryDataFunc(mtd *desc.MethodDescriptor, cd *runner.CallData) []byte {

	reqNum := int(cd.RequestNumber)
	modifyInventoryTransformBlockedRQ := &modifyinventory.TransformBlockedInventoryRQ{}

	if len(blockIds)<=numberOfRequestsR {
		var arr[] string
		if reqNum<len(blockIds) {
			arr = append(arr, blockIds[reqNum])
		}else {
			arr = append(arr, blockIds[0])
		}
		modifyInventoryTransformBlockedRQ.BlockIds = arr

	} else {
		if reqNum != (numberOfRequestsR-1) {
			modifyInventoryTransformBlockedRQ.BlockIds = blockIds[(reqNum * blockIdsPerReq):((reqNum * blockIdsPerReq) + blockIdsPerReq)]
		} else {
			modifyInventoryTransformBlockedRQ.BlockIds = blockIds[(reqNum * blockIdsPerReq):]
		}
	}

	modifyInventoryTransformBlockedRQ.ReferenceId = "b3059978-2a89-4538-ac05-a9a83d52e8eb"

	fmt.Print(reqNum)
	fmt.Print("  ")
	fmt.Print(modifyInventoryTransformBlockedRQ.BlockIds)
	fmt.Print("\n")

	binData, err := proto.Marshal(modifyInventoryTransformBlockedRQ)


	if(err != nil){
		fmt.Println(err.Error())
		os.Exit(1)
	}
	return binData
}

func releaseBlockedInventoryDataFunc(mtd *desc.MethodDescriptor, cd *runner.CallData) []byte {

	reqNum := int(cd.RequestNumber)
	modifyInventoryReleaseBlockedRQ := &modifyinventory.ReleaseBlockedInventoryRQ{}

	if len(blockIds)<=numberOfRequestsR {
		var arr[] string
		if reqNum<len(blockIds) {
			arr = append(arr, blockIds[reqNum])
		}else {
			arr = append(arr, blockIds[0])
		}
		modifyInventoryReleaseBlockedRQ.BlockIds = arr

	} else {
		if reqNum != (numberOfRequestsR-1) {
			modifyInventoryReleaseBlockedRQ.BlockIds = blockIds[(reqNum * blockIdsPerReq):((reqNum * blockIdsPerReq) + blockIdsPerReq)]
		} else {
			modifyInventoryReleaseBlockedRQ.BlockIds = blockIds[(reqNum * blockIdsPerReq):]
		}
	}

	modifyInventoryReleaseBlockedRQ.ReferenceId = "b3059978-2a89-4538-ac05-a9a83d52e8eb"

	fmt.Print(reqNum)
	fmt.Print("  ")
	fmt.Print(modifyInventoryReleaseBlockedRQ.BlockIds)
	fmt.Print("\n")

	binData, err := proto.Marshal(modifyInventoryReleaseBlockedRQ)


	if(err != nil){
		fmt.Println(err.Error())
		os.Exit(1)
	}
	return binData
}

func ModifyInventoryBlock(numberOfRequests int,
	numberOfConcurrentRequests uint,
	timePeriod string,
	flightNumbers[] string,
	bookingClass string,
	departureDateLocalStartsWith string,
	childCapacities[] string,
	infantCapacities[] string,
	adultCapacities[] string){

	timePeriodR = timePeriod
	flightNumbersR = flightNumbers
	bookingClassR = bookingClass

	existingDepartureDateLocalStartsWithR = departureDateLocalStartsWith
	layout := "2006-01-02T15:04:05"
	t, err := time.Parse(layout, departureDateLocalStartsWith)
	newFlightDepartureDate := t.AddDate(0, 0, numberOfRequests/len(flightNumbers))
	newDepartureDateLocalStartsWithR = newFlightDepartureDate.Format(layout)

	childCapacitiesR = convertToInt(childCapacities)
	adultCapacitiesR = convertToInt(adultCapacities)
	infantCapacitiesR = convertToInt(infantCapacities)

	fmt.Print("Modify Inventory calls with :\n\n")
	report, err := runner.Run(
		"aeroinventory.modifyinventory.ModifyInventoryService.BlockInventory",
		"localhost:6565",
		runner.WithProtoFile(
			"/home/kalana/codebases/goapp-kalana/goProtos/proto/modify_inventory.proto",
			[]string{}),
		runner.WithInsecure(true),
		runner.WithConcurrency(numberOfConcurrentRequests),
		runner.WithTotalRequests(uint(numberOfRequests)),
		runner.WithBinaryDataFunc(GetModifyInventoryBlockRequest),
	)

	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	report_generation.PrintReport(report)

	}

func ModifyInventoryTransform(numberOfRequests int,
	numberOfConcurrentRequests uint)  {
	numberOfRequestsR = numberOfRequests

	blockIds = databasecalls.GetAllBlockIds()

	if len(blockIds) == 0 {
		fmt.Println("No block ids found")
		os.Exit(1)
	}

	blockIdsPerReq = len(blockIds)/numberOfRequests
	if blockIdsPerReq == 0 {
		blockIdsPerReq = 1
	}
	fmt.Print("Transform Inventory calls with :\n\n")

	report, err := runner.Run(
		"aeroinventory.modifyinventory.ModifyInventoryService.TransformBlockedInventory",
		"localhost:6565",
		runner.WithProtoFile(
			"/home/kalana/codebases/goapp-kalana/goProtos/proto/modify_inventory.proto",
			[]string{}),
		runner.WithConcurrency(numberOfConcurrentRequests),
		runner.WithInsecure(true),
		runner.WithTotalRequests(uint(numberOfRequests)),
		runner.WithBinaryDataFunc(transformBlockedInventoryDataFunc))

	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	report_generation.PrintReport(report)
}

func ModifyInventoryReleaseBlocked(numberOfRequests int,
	numberOfConcurrentRequests uint)  {
	numberOfRequestsR = numberOfRequests

	blockIds = databasecalls.GetAllBlockIds()

	if len(blockIds) == 0 {
		fmt.Println("No block ids found")
		os.Exit(1)
	}

	blockIdsPerReq = len(blockIds)/numberOfRequests
	if blockIdsPerReq == 0 {
		blockIdsPerReq = 1
	}
	fmt.Print("Release Blocked Inventory calls with :\n\n")

	report, err := runner.Run(
		"aeroinventory.modifyinventory.ModifyInventoryService.ReleaseBlockedInventory",
		"localhost:6565",
		runner.WithProtoFile(
			"/home/kalana/codebases/goapp-kalana/goProtos/proto/modify_inventory.proto",
			[]string{}),
		runner.WithConcurrency(numberOfConcurrentRequests),
		runner.WithInsecure(true),
		runner.WithTotalRequests(uint(numberOfRequests)),
		runner.WithBinaryDataFunc(releaseBlockedInventoryDataFunc))

	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	report_generation.PrintReport(report)
}

func convertToInt(capacitiesInput[] string) []int {
	var convertedCapacityInputs []int
	for i := 0; i < len(capacitiesInput); i++ {
		number, err := strconv.ParseInt(capacitiesInput[i],10,64)

		if(err != nil){
			fmt.Println(err.Error())
			os.Exit(1)
		}

		convertedCapacityInputs = append(convertedCapacityInputs,int(number))
	}
	return convertedCapacityInputs
}