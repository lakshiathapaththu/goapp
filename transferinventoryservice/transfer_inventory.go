package transferinventoryservice

import (
	"com/accelaero/aeroinventory"
	transferInventory "com/accelaero/aeroinventory/transferinventory"
	"fmt"
	"github.com/bojand/ghz/runner"
	"github.com/jhump/protoreflect/desc"
	"google.golang.org/protobuf/proto"
	"os"
	"../report_generation"
	"strconv"
)

var fromFlightDepartureDateLocalTimeR string
var fromFlightNumberStartsWithR int64
var toFlightDepartureDateLocalTimeR string
var numberOfToFlightsPerFlightR int64

var bookingClassR string
var childSoldCapacityR int32
var infantSoldCapacityR int32
var adultSoldCapacityR int32

var childOnhCapacityR int32
var infantOnhCapacityR int32
var adultOnhCapacityR int32
var toFlightNumbersStartsWithR int64


func transferInvetDataFunc(mtd *desc.MethodDescriptor, cd *runner.CallData) []byte{

	req := &transferInventory.TransferInventoryRQ{}

	fmt.Print("Request number : ", cd.RequestNumber, "\n")

	fromFlight := &transferInventory.FlightDescriptor{}

	fromFlightNumber := fromFlightNumberStartsWithR+cd.RequestNumber
	toFlightNumberStartsWith  := toFlightNumbersStartsWithR+(cd.RequestNumber)*numberOfToFlightsPerFlightR

	fromFlight.FlightNumber = strconv.FormatInt(fromFlightNumber, 10)
	fromFlight.FlightNumber = "G9"+fromFlight.FlightNumber

	departureDateTimeCommonForFromFlight :=&aeroinventory.CommonDate{}
	departureDateTimeCommonForFromFlight.DateText = fromFlightDepartureDateLocalTimeR

	fromFlight.DepartureDateAndTimeLocal = departureDateTimeCommonForFromFlight
	req.FromFlight = fromFlight

	fmt.Print("From flight is ",fromFlight.FlightNumber, " ", departureDateTimeCommonForFromFlight.DateText,"\n")

	var flighTransferInventoryList []*transferInventory.FlightTransferInventory


	for i := 0; i < int(numberOfToFlightsPerFlightR); i++ {
		flightTransferInventory := &transferInventory.FlightTransferInventory{}
		toFlight := &transferInventory.FlightDescriptor{}

		flightNumber := toFlightNumberStartsWith+ int64(i)
		toFlight.FlightNumber = strconv.FormatInt(flightNumber, 10)
		toFlight.FlightNumber = "G9"+toFlight.FlightNumber

		departureDateTimeCommonForToFlight :=&aeroinventory.CommonDate{}
		departureDateTimeCommonForToFlight.DateText = toFlightDepartureDateLocalTimeR

		toFlight.DepartureDateAndTimeLocal = departureDateTimeCommonForToFlight

		flightTransferInventory.ToFlight = toFlight

		fmt.Print("To flight is ",i," ",toFlight.FlightNumber, " ", departureDateTimeCommonForToFlight.DateText,"\n")

		//sold Invet
		segmentSoldInvet := &transferInventory.SegmentTransferInventory{}
		segmentSoldInvet.SegmentCode = "SHJ/CMB"

		cabinSoldInvet := &transferInventory.CabinClassTransferInventory{}
		cabinSoldInvet.CabinClassCode = "Y"

		bcSubSoldInvet := &transferInventory.BookingClassSubtypeTransferInventory{}
		bcSubSoldInvet.BookingClassSubtypeCode = "STANDARD"

		bcSoldInvet := &transferInventory.BookingClassTransferInventory{}
		bcSoldInvet.BookingClassCode = bookingClassR
		bcSoldInvet.Adults = adultSoldCapacityR
		bcSoldInvet.Children = childSoldCapacityR
		bcSoldInvet.Infants = infantSoldCapacityR

		fmt.Print(adultSoldCapacityR, " ", childSoldCapacityR," ",infantSoldCapacityR, "\n")

		bcSubSoldInvet.BookingClassTransferInventories = []*transferInventory.BookingClassTransferInventory{bcSoldInvet}

		cabinSoldInvet.BookingClassSubtypeTransferInventories = []*transferInventory.BookingClassSubtypeTransferInventory{bcSubSoldInvet}

		segmentSoldInvet.CabinClassTransferInventories = []*transferInventory.CabinClassTransferInventory{cabinSoldInvet}


		//onhold invets
		segmentOnHoldInvet := &transferInventory.SegmentTransferInventory{}
		segmentOnHoldInvet.SegmentCode = "SHJ/CMB"

		cabinOnHoldInvet := &transferInventory.CabinClassTransferInventory{}
		cabinOnHoldInvet.CabinClassCode = "Y"

		bcSubOnHoldInvet := &transferInventory.BookingClassSubtypeTransferInventory{}
		bcSubOnHoldInvet.BookingClassSubtypeCode = "STANDARD"

		bcOnHoldInvet := &transferInventory.BookingClassTransferInventory{}
		bcOnHoldInvet.BookingClassCode = bookingClassR
		bcOnHoldInvet.Adults = adultOnhCapacityR
		bcOnHoldInvet.Children = childOnhCapacityR
		bcOnHoldInvet.Infants = infantOnhCapacityR

		fmt.Print(adultOnhCapacityR," ",childOnhCapacityR," ",infantOnhCapacityR,"\n")
		bcSubOnHoldInvet.BookingClassTransferInventories = []*transferInventory.BookingClassTransferInventory{bcOnHoldInvet}

		cabinOnHoldInvet.BookingClassSubtypeTransferInventories = []*transferInventory.BookingClassSubtypeTransferInventory{bcSubOnHoldInvet}

		segmentOnHoldInvet.CabinClassTransferInventories = []*transferInventory.CabinClassTransferInventory{cabinOnHoldInvet}

		flightTransferInventory.SoldInventories = []*transferInventory.SegmentTransferInventory{segmentSoldInvet}
		flightTransferInventory.OnHoldInventories = []*transferInventory.SegmentTransferInventory{segmentOnHoldInvet}

		//flighTransferInventoryList.
		flighTransferInventoryList = append(flighTransferInventoryList, flightTransferInventory)


		fmt.Print("\n")
	}

	req.FlightTransferInventories = flighTransferInventoryList

	fmt.Print("\n\n\n\n")

	binData, err := proto.Marshal(req)

	if(err != nil){
		fmt.Println(err.Error())
		os.Exit(1)
	}
	return binData
}

func TransferInventory(fromFlightDepartureDateLocalTime string,
	fromFlightNumberStartsWith int64,
	numberOfRequests int,
	toFlightDepartureDateLocalTime string,
	numberOfToFlightsPerFlight int64,
	bookingClass string,
	childSoldCapacity int32,
	infantSoldCapacity int32,
	adultSoldCapacity int32,
	childOnhCapacity int32,
	infantOnhCapacity int32,
	adultOnhCapacity int32)  {

	fromFlightDepartureDateLocalTimeR = fromFlightDepartureDateLocalTime
	fromFlightNumberStartsWithR = fromFlightNumberStartsWith
	toFlightDepartureDateLocalTimeR = toFlightDepartureDateLocalTime
	numberOfToFlightsPerFlightR = numberOfToFlightsPerFlight
	bookingClassR = bookingClass

	childSoldCapacityR =	childSoldCapacity
	infantSoldCapacityR = 	infantSoldCapacity
	adultSoldCapacityR =	adultSoldCapacity
	childOnhCapacityR =     childOnhCapacity
	infantOnhCapacityR =	infantOnhCapacity
	adultOnhCapacityR =	adultOnhCapacity

	toFlightNumbersStartsWithR = fromFlightNumberStartsWith+ int64(numberOfRequests)



	fmt.Print("Transfer Inventory calls with :\n\n")

	report, err := runner.Run(
		"aeroinventory.transferinventory.TransferInventoryService.TransferInventory",
		"localhost:6565",
		runner.WithProtoFile(
			"/home/chirantha/codebases/goProj/goProtos/proto/transfer_inventory.proto",
			[]string{}),
		runner.WithConcurrency(1),
		runner.WithInsecure(true),
		runner.WithTotalRequests(uint(numberOfRequests)),
		runner.WithBinaryDataFunc(transferInvetDataFunc),
	)

	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	report_generation.PrintReport(report)

	}
