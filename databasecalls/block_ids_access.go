package databasecalls

import (
	"database/sql"
	"fmt"
	"os"
	"strconv"
)

func GetAllBlockIds() []string{
	var blockIds[] string
	config, _ := LoadConfiguration("config.json")
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		config.Database.Host,
		config.Database.Port,
		config.Database.User,
		config.Database.Password,
		config.Database.Dbname)

	db, err := sql.Open("postgres", psqlInfo)
	userSql := "select  block_id from aeroinventory.t_inv_block_request;"
	rows, err := db.Query(userSql)


	for rows.Next() {
		var id int64
		err = rows.Scan(&id);
		if err != nil {
			os.Exit(1)
		}
		blockIds = append(blockIds, strconv.Itoa(int(id)))
	}

	if err != nil {
		fmt.Println(err.Error())
	}
	defer db.Close()

	return blockIds
}
